Le système cuisine
==================

Philippe Marquet, octobre 2021 — [CC BY](https://creativecommons.org/licenses/by/4.0/deed.fr)

> Activité débranchée de découverte de l'ordonnancement et de la synchronisation dans les systèmes d'exploitation au travers le fonctionnement de la cuisine d'un restaurant.

Un restaurant propose une liste de plats à sa carte.
Chacun de ces plats est le résultat de la préparation d'une recette.
La préparation d'une recette – par exemple celle de la tarte aux abricots – consiste à suivre différentes étapes.
Chaque étape de la préparation peut nécessiter des ingrédients – par exemple des abricots, du sucre, une pâte brisée –.
Elle peut également nécessiter l'utilisation d'ustensiles – par exemple un mixeur, un four –.

Un certain nombre de cuisinières et cuisiniers travaillent dans la cuisine du restaurant.
Sous la direction d'un chef de cuisine, elles et ils prennent en charge la préparation des recettes.
Ces préparations consistent à réaliser à la suite les unes des autres les différentes étapes des recettes.
 
Il est nécessaire d'organiser l'ensemble de ce travail en cuisine :

* les ingrédients nécessaires à chacun.
  Ceux-ci peuvent provenir du stock – comme les abricots ou le sucre –, ou être le résultat de la réalisation d'autres recettes – comme une pâte à tarte –.
  Ils peuvent être disponibles ou non. 
  Il est donc possible qu'un cuisinier doive attendre l'approvisionnement d'un ingrédient, ou attendre qu'une recette soit terminée.
  Le chef va alors lui faire prendre en charge la préparation d'une autre recette.
* l'usage des ustensiles. 
  Certains sont partagés, et ne peuvent être utilisés que par une personne à la fois.
  Il est donc possible qu'un cuisinier doive attendre qu'un ustensile soit disponible. 
  Le chef va alors lui faire prendre en charge la préparation d'une autre recette.

Le principe des activités proposées est d'observer ce fonctionnement et de faire le parallèle avec la gestion des processus et des ressources dans un système d'exploitation.

### Note de rédaction ###

**TODO**

* trouver un terme épicène pour désigner une cuisinière ou un cuisinier. Trouver un autre terme pour chef (qui est pourtant épicène), chef de cuisine plutôt que chef cuisinier.
* compléter d'un glossaire de la cuisine ? Au moins le terme _brigade_ ? 

→ Recettes
----------

Quelques recettes, à aménager – ré-écrire –, pour concrétiser l'activité.

#### Tarte aux abricots ####

* dénoyauter 1kg d'abricots et les couper en deux
* garnir un moule d'une pâte brisée
* disposer les abricots sur la pâte à tarte
* cuire la tarte à four chaud pendant 15 min.
* préparer un appareil : mélanger au batteur-mixeur 3 œufs, 20cl de crème, 40g de poudre d'amandes, 100g de sucre
* couvrir les abricots avec l'appareil
* poursuivre la cuisson pendant 15 min.

#### Quiche lorraine ####

* disposer une pâte brisée ou pâte feuilletée dans un moule
* préparer au batteur-mixeur un appareil à base d'œufs, de crème fraîche et de lardons
* enfourner à four chaud pendant 30 minutes

#### Pâte brisée ####

* ingrédients : 250 g de farine, 125 g de beurre mou, 1 œuf, 2 cl d'eau, une pincée de sel
* mélanger la farine, le beurre et le sel au batteur-mixeur
* ajouter œuf et eau, mélanger jusqu'à formation d'un boule
* disposer sur le plan de travail fariné, former un boule, garder 30 minutes au réfrigérateur avant utilisation 

→ Cuisine et système
--------------------

La similitude entre la gestion des processus et des ressources dans un système d'exploitation d'une part, et l'organisation de la cuisine d'un restaurant est basée sur les grandes lignes suivantes :

En terme de programmes, processus, et processeurs :

* les recettes sont comme les _programmes_.
  Elles sont constituées de suites d'instructions.
* la préparation d'une recette est similaire à un _processus_.
  C'est une réalisation d'une recette, comme une processus est une réalisation d'un programme.
  Elle a un début, elle est caractérisé à un instant donné par sont état d'avancement, elle peut être bloquée – par exemple parce qu'un ingrédient manque pour le moment, ou parce qu'un ustensile n'est pas disponible –, elle se terminera – et le plat sera prêt !
  Elle aussi peut simplement être interrompue, laissée de côté, et reprise ensuite. 
* les cuisiniers et cuisinières de la brigade sont comme les _processeurs_.
  Ce sont eux qui vont prendre en charge la préparation des recettes, comme les processeurs prennent en charge les processus, c'est-à-dire l'exécution effective des programmes. 

En terme d'entrée/sorties :

* les ingrédients sont comme des _données_.
  Ils sont utilisées dans certaines étapes de la préparation des recettes.
* les plats, résultats de la préparation des recettes, sont également des _données_.
  Ils sont soit le résultat final attendu par la commande d'un client, mais aussi le résultat attendu comme un ingrédient pour une autre recette.
* ces ingrédients ou plats doivent être stockées dans des contenants, comme les données dans un _fichier_ ou un _disque_.
  Ces contenants peuvent être disponibles – ou pas –, peuvent être pleins, ou vides.
  La préparation d'une recette peut donc être bloquée en attente de la disponibilité d'un ingrédient dans un contenant particulier, comme un processus peut être bloqué en l'attente de la lecture d'une donnée depuis un fichier particulier.

En terme de ressources

* les ustensiles sont comme les _périphériques_.
  Ils sont nécessaires à certaines étapes de la préparation des recettes.
  Ils peuvent être disponibles, ou non.
  La préparation de la recette en vient alors à être suspendue en attente de la disponibilité de l'ustensile, comme un processus en vient a être bloqué en l'attente de la disponibilité du périphérique.
* un cuisinier est une « ressource » particulière, comme le _processeur_ est une ressource particulière.
  Un cuisinier ne sera pas mobilisé pour attendre la disponibilité d'une ressource, comme un processeur ne sera pas mobilisé par un processus bloqué en l'attente d'un périphérique.
  À un instant donné, un cuisinier travaillera à la préparation d'une des recettes pour lesquelles l'ensemble des ustensiles sont disponibles, comme un processeur exécutera les instructions d'un des processus prêts.
* partage


En terme d'ordonnancement

* le chef cuisinier est comme l'_ordonnanceur_ du système d'exploitation.
  Il est en charge d'affecter la préparation des recettes aux cuisiniers, comme l'ordonnancement est en charge d'affecter les processus aux processeurs. 
  Plus précisément, il est en charge d'affecter à chaque cuisinier, à chaque instant, la préparation d'une recette.
  Il peut interrompre un cuisinier dans la préparation d'une recette pour le faire travailler à la préparation d'une autre recette.
* la préparation d'une recette est comme un _processus_.
  <!-- Elle ne progresse que si elle est prise en charge par un cuisinier, comme un processus ne progresse que s'il est pris en charge par un processeur. -->
  Le travail du cuisiner sur la préparation peut être interrompu par le chef cuisinier, comme l'exécution d'un processus sur un processeur peut être interrompue par l'ordonnanceur. 

En terme de synchronisation

* synchronisation par les données.
  Consommation d'ingrédients blocante.
  Production d'ingrédients débloque ?
  **TODO** Mécanisme à trouver
* synchronisation pour les accès aux ustensiles. 
  À chaque ustensile est associé un système de réservation, comme un _verrou_, mécanisme de synchronisation dans les systèmes d'exploitation.
  L'utilisation de l'ustensile passe par une réservation préalable, qui peut être blocante parce que l'ustensile est occupé par un autre cuisinier. 
  **TODO** Quelle « primitive » pour l'utilisation d'ustensile ? (prendre / relâcher, avec prendre bloquant ?)

De synchronisation à interblocage **TODO**

* des recettes dont de possibles préparations (mais pas toutes) peuvent mener à interblocage
* idée que l'interblocage est dû à des « erreurs » dans les recettes, pas dans les ordres du chef

→ Activités
-----------

Possibles activités à partir de cette métaphore

* faire la cuisine et déguster les plats
* présenter le système cuisine en introduction à un cours de système d'exploitation, à la partie "Ordonnancement et synchronisation" d'un cours de système d'exploitation
* jeu de rôle du système cuisine
* simulateur du système cuisine à base de processus.  

### Jeu de rôle du système cuisine ###

En vrac : les joueurs sont des cuisiniers ; les ingrédients sont des pions ; les contenants sont des ... contenants de pions ; les ustensiles sont matérialisées comme tels.

À une préparation doit être attachée :

* un éventuel cuisinier, la préparation de la recette pourra alors progresser
* la recette qui est préparée – éventuellement plusieurs préparations en cours de la même recette (partage donc la recette qui existe / est écrite en un unique exemplaire ?)
* un état d'avancement dans la recette en préparation (l'« instruction » en cours)
* les ustensiles utilisés à l'instant présent
* l'ustensile nécessaire à la progression de la recette et qui n'est pas disponible

**TODO** Reste donc à trouver le bon « matériel » pour tout cela.

Jouer = avancer dans la préparation de sa recette

* avec un tour de jeu ?
* sans tour de jeu → asynchronisme ? Il faut alors que les actions élémentaires des recettes prennent un certain temps (mécanisme à base de cartes / de lancer de dés ?)

Chef cuisinier :

* ce n'est pas un joueur
* différents algorithme d'« élection » ?

Recette

* faites d'actions atomiques simples.
  **TODO** simplifier les recettes exemples données plus haut. 

### Simulateur du système cuisine à base de processus ###

Par exemple en Python – plus facilement abordable ? –, ou en C.

* les cuisiniers sont des processus (ou des threads).
* les contenants sont des variables - ou des fichiers – partagés par les processus.
* des verrous sont utilisés pour gérer le partage des ustensiles et les accès aux (contenants des) ingrédients.
* des recettes sont des suite d'instructions élémentaires = de fonctions qui réalisent les étapes, comme des accesseurs aux ingrédients et ustensiles...

**TODO** ...

Aussi 
=====

- Débuter avec un unique cuisinier (préémptible)
- Illustrer le non déterminisme avec les dates de sorties des plats
- Illustrer la notion de famine avec un cuisinier qui produit une recette en boucle
- Illustrer la notion d'interblocage avec deux recettes qui sollicitent deux ressources dans un ordre différents pour que chaque cuisinier soit en attente de l'autre

