Attribution 4.0 International (CC BY 4.0)

# Résumé de la licence CC BY 

Vous êtes autorisé à :

    Partager — copier, distribuer et communiquer le matériel par tous moyens et sous tous formats
    Adapter — remixer, transformer et créer à partir du matériel
    pour toute utilisation, y compris commerciale.

Selon les conditions suivantes :

    Attribution — Vous devez créditer l'Œuvre, intégrer un lien vers la licence et indiquer si des modifications ont été effectuées à l'Oeuvre. Vous devez indiquer ces informations par tous les moyens raisonnables, sans toutefois suggérer que l'Offrant vous soutient ou soutient la façon dont vous avez utilisé son Oeuvre.

    Pas de restrictions complémentaires — Vous n'êtes pas autorisé à appliquer des conditions légales ou des mesures techniques qui restreindraient légalement autrui à utiliser l'Oeuvre dans les conditions décrites par la licence.

# Licence publique Creative Commons Attribution 4.0 International

Disponible à https://creativecommons.org/licenses/by/4.0/legalcode.fr
